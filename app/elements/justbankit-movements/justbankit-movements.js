{
  const {
    html,
  } = Polymer;

  class JustbankitMovements extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'justbankit-movements';
    }

    static get properties() {
      return {
        selectedAccount: {
          type: Object,
          value: {}
        },
        movementsSelectedAccount: {
          type: Array,
          value: []
        },
        hostEndpoint: {
          type: String,
          value: window.AppConfig.apiV1Host
        },
        pathEndpointMovements: {
          type: String,
          value: ''
        },
        isLoading: {
          type: Boolean,
          value: false
        },
        lockedStyle: {
          type: String,
          value: ''
        }

      };
    }

    load(isActive) {

      if (isActive.value) {
        console.log('Page state Active');

        console.log('Page state /selectedAccount: ' + JSON.stringify(this.selectedAccount));

        this.isLoading = true;
        this.lockedStyle = window.AppConfig.cssLockedElement;

        this.pathEndpointMovements = window.AppConfig.apiV1UrlBase + window.AppConfig.apiV1UrlAccounts + this.selectedAccount._id.$oid + '/' + window.AppConfig.apiV1UrlMovements;

        this.$.jbDpGetMovements.body = {};
        this.$.jbDpGetMovements.generateRequest();

      } else {
        console.log('Page state inactive');
      }
    }


    _computeFormat(number) {
      return (parseFloat(number).toFixed(2));
    }


    _handleRequestMovementsSuccess(evt) {
      console.log('_handleRequestMovementsSuccess');

      const movementsAccount = evt.detail;

      var movementsDisplayed = movementsAccount.map(movement => ({
        'date': movement.timestamp,
        'label': movement.idType,
        'description': movement.description,
        'parsedAmount': Number(movement.amount)
      }));

      console.log(movementsDisplayed);
      this.movementsSelectedAccount = movementsDisplayed;

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;
    }

    _handleRequestMovementsError(evt) {
      console.log('_handleRequestMovementsError');

      this.getMsg('justbankit-movements-error')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'error',
            message: translation
          });
        });

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;

    }


    _handleAddMovementClick(evt) {
      console.log('_handleAddMovementClick');


      this.dispatchEvent(
        new Event('movements-add-clicked', {
          composed: true,
          bubbles: true
        })
      );
    }

    _handleBackClick(evt) {
      console.log('_handleBackClick');

      this.dispatchEvent(
        new Event('movements-back-ok', {
          composed: true,
          bubbles: true
        })
      );

    }


  }

  customElements.define(JustbankitMovements.is, JustbankitMovements);
}
