{
  const {
    html
  } = Polymer;
  class JustbankitLogin extends Polymer.mixinBehaviors(
    [ CellsBehaviors.i18nBehavior ],
    Polymer.Element
  ) {
    static get is() {
      return 'justbankit-login';
    }

    static get properties() {
      return {
        loggedInUser: {
          type: Object,
          value: {}
        },
        validations: {
          type: Object,
          value: {
            autoValidate: false,
            inputStatusValidate: true,
            inputType: 'text',
            allowedValue: '^[0-9]{8,8}[A-Za-z]$',
            errorMessage: 'rutMsg',
            errorMessageIcon: 'coronita:error',
            maxLength: '9'
          }
        },
        isLoading: {
          type: Boolean,
          value: false
        },
        lockedStyle: {
          type: String,
          value: ''
        },
        hostEndpoint: {
          type: String,
          value: window.AppConfig.apiV1Host
        },
        pathEndpointLogin: {
          type: String,
          value: window.AppConfig.apiV1UrlBase + window.AppConfig.apiV1UrlLogin

        }
      };
    }


    load(isActive) {

      if (isActive.value) {
        console.log('Page state Active');

        this.isLoading = false;
        this.lockedStyle = window.AppConfig.cssUnLockedElement;

        var valueUserIdCard = '';
        var valueUserName = '';
        const objLocalStor = localStorage.getItem(window.AppConfig.localStorageLoggedInUser);

        if (objLocalStor) {
          const jObjLocalStor = JSON.parse(objLocalStor);
          valueUserIdCard = jObjLocalStor.idCard;
          valueUserName = jObjLocalStor.name;
        }

        this.$.jbFrmLogin.userId = valueUserIdCard;
        this.$.jbFrmLogin.userName = valueUserName;
        this.$.jbFrmLogin.userPassword = '';
      } else {
        console.log('Page state inactive');
      }
    }


    _handleLoginButtonClicked(evt) {
      console.log('_handleLoginButtonClicked');

      this.isLoading = true;
      this.lockedStyle = window.AppConfig.cssLockedElement;

      this.$.jbDpLogin.body = {
        id: this.$.jbFrmLogin.userId,
        password: this.$.jbFrmLogin.userPassword
      };
      this.$.jbDpLogin.generateRequest();
    }


    _handleRequestLoginSuccess(evt) {
      console.log('_handleRequestLoginSuccess');

      const objLoggedInUser = evt.detail.user;

      const objToLocalStore = {
        idCard: objLoggedInUser.login.id,
        name: objLoggedInUser.personal.first_name
      };

      localStorage.setItem(
        window.AppConfig.localStorageLoggedInUser,
        JSON.stringify(objToLocalStore)
      );

      sessionStorage.setItem(window.AppConfig.sessionToken, evt.detail.token);
      sessionStorage.setItem(window.AppConfig.sessionIdCardUser, evt.detail.user.login.id);

      const detail = objLoggedInUser;
      this.dispatchEvent(
        new CustomEvent('login-login-success', {
          composed: true,
          bubbles: true,
          detail
        })
      );

    }


    _handleRequestLoginError(evt) {
      console.log('_handleRequestLoginError');

      this.getMsg('justbankit-login-error')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'error',
            message: translation
          });
        });

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;
    }


    _handleChangeUserButtonClicked(evt) {
      console.log('_handleChangeUserButtonClicked');

      this.$.jbFrmLogin.userId = '';
      this.$.jbFrmLogin.userName = '';
      this.$.jbFrmLogin.userPassword = '';

      localStorage.removeItem(window.AppConfig.localStorageLoggedInUser);
      sessionStorage.removeItem(window.AppConfig.sessionToken);
    }

    _handleRegisterButtonClicked(evt) {


      console.log('_handleRegisterButtonClicked');

      this.dispatchEvent(
        new Event('login-register-clicked', {
          composed: true,
          bubbles: true
        })
      );

    }

  }

  customElements.define(JustbankitLogin.is, JustbankitLogin);
}
