{
  const { html } = Polymer;

  class JustbankitAccounts extends Polymer.mixinBehaviors(
    [ CellsBehaviors.i18nBehavior ],
    Polymer.Element
  ) {
    static get is() {
      return 'justbankit-accounts';
    }

    static get properties() {
      return {
        loggedInUser: {
          type: Object,
          value: {}
        },
        userAccounts: {
          type: Array,
          value: []
        },
        hostEndpoint: {
          type: String,
          value: window.AppConfig.apiV1Host
        },
        pathEndpointAccounts: {
          type: String,
          value: ''
        },
        header: {
          type: String,
          value: ''
        },
        isLoading: {
          type: Boolean,
          value: false
        },
        lockedStyle: {
          type: String,
          value: ''
        }
      };
    }

    load(isActive) {
      if (isActive.value) {
        console.log('Page state Active');

        console.log(
          'Page state /loggedInUser: ' + JSON.stringify(this.loggedInUser)
        );
        this.isLoading = true;
        this.lockedStyle = window.AppConfig.cssLockedElement;

        this.pathEndpointAccounts =
          window.AppConfig.apiV1UrlBase +
          window.AppConfig.apiV1UrlUsers +
          this.loggedInUser._id.$oid +
          '/' +
          window.AppConfig.apiV1UrlAccounts;

        /*
        this.$.jbDpGetAccounts.headers = {
          authorization : 'JWT ' + sessionStorage.getItem('token'),
              //  authorization : sessionStorage.getItem(window.AppConfig.sessionToken),
               iduser : parseInt(sessionStorage.getItem(window.AppConfig.sessionIdCardUser))
        };
*/
        this.$.jbDpGetAccounts.body = {};
        this.$.jbDpGetAccounts.generateRequest();
      } else {
        console.log('Page state inactive');
      }
    }

    _handleRequestAccountsSuccess(evt) {
      console.log('_handleRequestAccountsSuccess');

      this.accountsUsers = evt.detail;

      var accountsDisplayed = this.accountsUsers.map(account => ({
        imgSrc:
          window.AppConfig.staticV1Host +
          window.AppConfig.staticV1UrlImages +
          'icon_cuenta_' +
          account.idType +
          '.png',
        imgWidth: 122,
        imgHeight: 148,
        id: account._id.$oid,
        name: `${account.alias}`,
        headingLevel: 7,
        description: {
          value: account.iban + ' / ' + account.idType,
          masked: false
        },
        primaryAmount: {
          amount: parseFloat(account.balance).toFixed(2),
          currency: `${account.idBadge}`
        }
      }));
      this.$.lstAccounts.items = accountsDisplayed;

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;
    }

    _handleRequestAccountsError(evt) {
      console.log('_handleRequestAccountsError');

      this.getMsg('justbankit-accounts-error').then(translation => {
        this.$.toaster.showToast({
          type: 'error',
          message: translation
        });
      });

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;
    }

    _handleAccountTap(evt) {
      console.log('_handleAccountTap');
      console.log(evt);

      const accountSelectedOid = evt.detail.productId;

      const accountSelected = this.accountsUsers
        .filter(account => account._id.$oid === accountSelectedOid)
        .reduce(obj => obj[0]);
      console.log(accountSelected);

      const detail = accountSelected;

      this.dispatchEvent(
        new CustomEvent('accounts-view-movements-clicked', {
          composed: true,
          bubbles: true,
          detail
        })
      );
    }

    _handleAddAccountClick(evt) {
      console.log('_handleAddAccountClick');
      console.log(
        'Page state /loggedInUser: ' + JSON.stringify(this.loggedInUser)
      );

      this.dispatchEvent(
        new Event('accounts-add-clicked', {
          composed: true,
          bubbles: true
        })
      );
    }
  }

  customElements.define(JustbankitAccounts.is, JustbankitAccounts);
}
