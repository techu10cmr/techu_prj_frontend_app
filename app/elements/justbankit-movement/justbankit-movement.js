{
  const {
    html,
  } = Polymer;
  class JustbankitMovement extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'justbankit-movement';
    }

    static get properties() {

      return {
        selectedAccount: {
          type: Object,
          value: {}
        },
        movementTypes: {
          type: Array,
          value: [{
            label: 'ingreso',
            value: 'ingreso',
          }, {
            label: 'reintegro',
            value: 'reintegro',
          }]
        },
        hostEndpoint: {
          type: String,
          value: window.AppConfig.apiV1Host
        },
        pathEndpointMovement: {
          type: String,
          value: ''
        },
        isLoading: {
          type: Boolean,
          value: false
        },
        lockedStyle: {
          type: String,
          value: ''
        },
        dateToAdd: {
          type: Date
        }

      };
    }

    load(isActive) {

      if (isActive.value) {
        console.log('Page state Active');

        console.log('Page state /selectedAccount: ' + JSON.stringify(this.selectedAccount));
        this.isLoading = false;
        this.lockedStyle = window.AppConfig.cssUnLockedElement;

        this.pathEndpointMovements = window.AppConfig.apiV1UrlBase + window.AppConfig.apiV1UrlAccounts + this.selectedAccount._id.$oid + '/' + window.AppConfig.apiV1UrlMovements;

        this.$.jbDtCreationDate.reset();
        this.$.jbSlcTypesMovs.selected = -1;
        this.$.jbTxtAmount.value = '';
        this.$.jbTxtConcepto.value = '';

      } else {
        console.log('Page state inactive');
      }
    }


    _handleDateChanged(evt) {
      console.log('_handleDateChanged');

      this.dateToAdd = evt.detail + 'T00:00:00+00:00';
      console.log(this.dateToAdd);
    }


    _handleConfirmClick(evt) {
      console.log('_handleConfirmClick');

      this.isLoading = true;
      this.lockedStyle = window.AppConfig.cssLockedElement;

      const movementToAdd = {
        'timestamp': this.dateToAdd,
        'idType': this.$.jbSlcTypesMovs.value,
        'description': this.$.jbTxtConcepto.value,
        'amount': Number(this.$.jbTxtAmount.value)
      };

      console.log(movementToAdd);

      this.$.jbDpAddMovement.body = movementToAdd;
      this.$.jbDpAddMovement.generateRequest();
    }

    _handleCancelClick(evt) {
      console.log('_handleCancelClick');

      this.dispatchEvent(
        new Event('movement-back-ok', {
          composed: true,
          bubbles: true
        })
      );

    }


    _handleRequestAddMovementSuccess(evt) {
      console.log('_handleRequestAddMovementSuccess');

      this.getMsg('justbankit-movement-back-ok')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'success',
            message: translation
          });


        });


      this.dispatchEvent(
        new Event('movement-back-ok', {
          composed: true,
          bubbles: true
        })
      );

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;

    }

    _handleRequestAddMovementError(evt) {
      console.log('_handleRequestAddMovementError');

      this.getMsg('justbankit-movement-add-error')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'error',
            message: translation
          });
        });

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;

    }


  }

  customElements.define(JustbankitMovement.is, JustbankitMovement);
}
