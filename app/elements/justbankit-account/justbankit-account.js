{
  const {
    html,
  } = Polymer;
  class JustbankitAccount extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'justbankit-account';
    }

    static get properties() {

      return {
        loggedInUser: {
          type: Object,
          value: {}
        },
        accountTypes: {
          type: Array,
          value: [{
            label: 'credito',
            value: 'credito',
          }, {
            label: 'personal',
            value: 'personal',
          }, {
            label: 'ahorro',
            value: 'ahorro',
          }]
        },
        badgeTypes: {
          type: Array,
          value: [{
            label: 'Euro',
            value: 'EUR',
          }, {
            label: 'Dólar',
            value: 'USD',
          }, {
            label: 'Yen',
            value: 'JPY',
          }]
        },
        hostEndpoint: {
          type: String,
          value: window.AppConfig.apiV1Host
        },
        pathEndpointAccount: {
          type: String,
          value: ''
        },
        isLoading: {
          type: Boolean,
          value: false
        },
        lockedStyle: {
          type: String,
          value: ''
        }
      };
    }

    load(isActive) {

      if (isActive.value) {
        console.log('Page state Active');

        console.log('Page state /loggedInUser: ' + JSON.stringify(this.loggedInUser));
        this.isLoading = false;
        this.lockedStyle = window.AppConfig.cssUnLockedElement;

        this.pathEndpointAccount = window.AppConfig.apiV1UrlBase + window.AppConfig.apiV1UrlUsers + this.loggedInUser._id.$oid + '/' + window.AppConfig.apiV1UrlAccounts;

        this.$.jbSlcTypesAccs.selected = -1;
        this.$.jbSlcTypesBadge.selected = -1;
        this.$.jbAlias.value = '';


      } else {
        console.log('Page state inactive');
      }
    }


    _handleConfirmClick(evt) {
      console.log('_handleConfirmClick');

      this.isLoading = true;
      this.lockedStyle = window.AppConfig.cssLockedElement;

      const accountToAdd = {
        'idType': this.$.jbSlcTypesAccs.value,
        'idBadge': this.$.jbSlcTypesBadge.value,
        'alias': this.$.jbAlias.value
      };

      this.$.jbDpAddAccount.body = accountToAdd;
      this.$.jbDpAddAccount.generateRequest();
    }


    _handleCancelClick(evt) {
      console.log('_handleCancelClick');

      this.dispatchEvent(
        new Event('account-back-ok', {
          composed: true,
          bubbles: true
        })
      );

    }


    _handleRequestAddAccountSuccess(evt) {
      console.log('_handleRequestAddAccountSuccess');

      this.getMsg('justbankit-account-add-ok')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'success',
            message: translation
          });


        });

      this.dispatchEvent(
        new Event('account-back-ok', {
          composed: true,
          bubbles: true
        })
      );

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;

    }

    _handleRequestAddAccountError(evt) {
      console.log('_handleRequestAddAccountError');

      this.getMsg('justbankit-account-add-error')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'error',
            message: translation
          });
        });

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;

    }


  }

  customElements.define(JustbankitAccount.is, JustbankitAccount);
}
