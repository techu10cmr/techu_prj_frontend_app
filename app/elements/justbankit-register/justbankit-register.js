{
  const {
    html,
  } = Polymer;
  class JustbankitRegister extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'justbankit-register';
    }

    static get properties() {

      return {
        hostEndpoint: {
          type: String,
          value: window.AppConfig.apiV1Host
        },
        pathEndpointUser: {
          type: String,
          value: ''
        },
        isLoading: {
          type: Boolean,
          value: false
        },
        lockedStyle: {
          type: String,
          value: ''
        }


      };
    }

    load(isActive) {

      if (isActive.value) {
        console.log('Page state Active');

        this.pathEndpointUsers = window.AppConfig.apiV1UrlBase + window.AppConfig.apiV1UrlUsers;

        this.isLoading = false;
        this.lockedStyle = window.AppConfig.cssUnLockedElement;

        this.$.jbIdCard.value = '';
        this.$.jbIdPassword.value = '';
        this.$.jbFirstName.value = '';

        this.$.jbLastName.value = '';
        this.$.jbMobilePhone.value = '';
        this.$.jbEmail.value = '';
        this.$.jbStreet.value = '';
        this.$.jbPostcode.value = '';
        this.$.jbCity.value = '';

      } else {
        console.log('Page state inactive');
      }
    }


    _handleConfirmClick(evt) {
      console.log('_handleConfirmClick');

      this.isLoading = true;
      this.lockedStyle = window.AppConfig.cssLockedElement;

      const userToAdd = {
        'login': {
          'id': this.$.jbIdCard.value,
          'password': this.$.jbIdPassword.value
        },
        'personal': {
          'first_name': this.$.jbFirstName.value,
          'last_name': this.$.jbLastName.value
        },
        'contact': {
          'mobilePhone': this.$.jbMobilePhone.value,
          'email': this.$.jbEmail.value,
          'address': {
            'street': this.$.jbStreet.value,
            'postcode': this.$.jbPostcode.value,
            'city': this.$.jbCity.value
          }
        }
      };

      console.log(userToAdd);

      this.$.jbDpAddUser.body = userToAdd;
      this.$.jbDpAddUser.generateRequest();
    }

    _handleCancelClick(evt) {
      console.log('_handleCancelClick');

      this.dispatchEvent(
        new Event('register-back-ok', {
          composed: true,
          bubbles: true
        })
      );

    }


    _handleRequestAddUserSuccess(evt) {
      console.log('_handleRequestAddUserSuccess');

      this.getMsg('justbankit-register-add-ok')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'success',
            message: translation
          });


        });

      this.dispatchEvent(
        new Event('register-back-ok', {
          composed: true,
          bubbles: true
        })
      );

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;

    }

    _handleRequestAddUserError(evt) {
      console.log('_handleRequestAddMovementError');

      this.getMsg('justbankit-register-add-error')
        .then(translation => {
          this.$.toaster.showToast({
            type: 'error',
            message: translation
          });
        });

      this.isLoading = false;
      this.lockedStyle = window.AppConfig.cssUnLockedElement;

    }


  }
  customElements.define(JustbankitRegister.is, JustbankitRegister);

}
