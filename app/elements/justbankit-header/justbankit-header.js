{
  const {
    html
  } = Polymer;

  class JustbankitHeader extends Polymer.mixinBehaviors(
    [ CellsBehaviors.i18nBehavior ],
    Polymer.Element
  ) {
    static get is() {
      return 'justbankit-header';
    }

    static get properties() {
      return {
        loggedInUser: {
          type: Object,
          observer: '_loggedInUserChanged'
        },
        hostEndpoint: {
          type: String,
          value: window.AppConfig.apiV1Host
        },
        pathEndpointLogout: {
          type: String,
          value: ''
        },
        iconLogout: {
          type: String,
          value: ''
        },
        iconSettings: {
          type: String,
          value: ''
        }
      };
    }

    _loggedInUserChanged(newUser, oldUser) {
      console.log('_loggedInUserChanged');

      console.log(newUser);
      console.log(oldUser);

      console.log(this.loggedInUser);

      this.iconLogout = (newUser) ? 'coronita:noclient' : '';
      this.iconSettings = (newUser) ? 'coronita:settings' : '';
      this.urlIconHome = (newUser) ? 'accounts' : 'login';
      this.pathEndpointLogout = (newUser) ? window.AppConfig.apiV1UrlBase + window.AppConfig.apiV1UrlLogout + this.loggedInUser._id.$oid : '';
    }

    _handleLogoutButtonPressed() {
      console.log('_handleLogoutButtonPressed');

      this.isLoading = true;
      this.lockedStyle = window.AppConfig.cssLockedElement;

      this.$.jbDpLogout.body = {};
      this.$.jbDpLogout.generateRequest();
    }

    _handleRequestLogoutSuccess(evt) {
      console.log('_handleRequestLogoutSuccess');

      this.loggedInUser = null;
      sessionStorage.removeItem(window.AppConfig.sessionToken);

      this.dispatchEvent(
        new CustomEvent('header-logout-success', {
          composed: true,
          bubbles: true
        })
      );
    }

  }

  customElements.define(JustbankitHeader.is, JustbankitHeader);
}
